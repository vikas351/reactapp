import React from 'react';
import ReactDOM from 'react-dom';
import App from './components/App.jsx';
import Navigation from './components/navigation.jsx';

ReactDOM.render(<div><App /><Navigation/></div>
                ,document.getElementById('app'));