import React from 'react';

var App = React.createClass({
   getInitialState(){
      return {
          header: "Header from state 22222...",
         "content": "Content from state 3333..."
      }
   },
   render() {
      var elm =  (
         <div>
            <h1>{this.state.header}</h1>
            <h2>{this.state.content}</h2>
         </div>
      )
      return elm;
   }
})

/*class App extends React.Component {
   constructor(props) {
      super(props);
		
      this.state = {
         header: "Header from state...",
         "content": "Content from state..."
      }
   }
	
   render() {
      return (
         <div>
            <h1>{this.state.header}</h1>
            <h2>{this.state.content}</h2>
         </div>
      );
   }
}*/

export default App;