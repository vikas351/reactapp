import React from 'react';

var Navigation = React.createClass({
   getInitialState(){
      return {
            'name':"navigation",
            'navList':[
               'about',
               'home',
               'my checkins'
            ]
               }
   },
   eachList(listItem){
                return (<li ><a key="" href="#">
                            {listItem}</a>
                        </li>);    
            },
   render() {
      var elm =  (
         <nav>
            <h1>{this.state.name}</h1>
            <ul>{this.state.navList.map(this.eachList)}</ul>
         </nav>
      )
      return elm;
   }
})

export default Navigation;